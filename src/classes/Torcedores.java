/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.ArrayList;

/**
 *
 * @author ideiah
 */
public class Torcedores {

    private ArrayList<Torcedor> arrayTorcedores;

    /**
     * Construtor que inicia novo array
     *
     * @param arrayTorcedores
     */
    public Torcedores(ArrayList<Torcedor> arrayTorcedores) {
        this.arrayTorcedores = arrayTorcedores;
    }

    public Torcedores() {
        arrayTorcedores = new ArrayList<Torcedor>();
    }

    public ArrayList<Torcedor> getArrayTorcedores() {
        return arrayTorcedores;
    }

    public void setArrayTorcedores(ArrayList<Torcedor> arrayTorcedores) {
        this.arrayTorcedores = arrayTorcedores;
    }

    public void cadastrarTorcedores(Torcedor torcedor) {
        this.arrayTorcedores.add(torcedor);
    }

    public ArrayList<String> escrever() {
        ArrayList<String> linhas = new ArrayList();
        for (int i = 0; i < arrayTorcedores.size(); i++) {
            linhas.add(arrayTorcedores.get(i).getNome() + "," + arrayTorcedores.get(i).getDataNasc() + "," + arrayTorcedores.get(i).getCpf() + "," + arrayTorcedores.get(i).getRg() + "," + arrayTorcedores.get(i).getEndereco().getBairro() + "," + arrayTorcedores.get(i).getEndereco().getRua() + "," + arrayTorcedores.get(i).getEndereco().getNum() + "," + arrayTorcedores.get(i).getNaturalidade().getEstado() + "," + arrayTorcedores.get(i).getNaturalidade().getCidade() + "," + arrayTorcedores.get(i).getSelecao() + "," + arrayTorcedores.get(i).getTime() + "," + arrayTorcedores.get(i).getUsuarioCadastrou());
        }
        return linhas;
    }
}
