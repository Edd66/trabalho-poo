/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author ideiah
 */
public class Usuario{

    private String login;
    private String senha;

    /**
     * Cosntrutor da classe usuario
     *
     * @param login
     * @param senha
     * @param nome
     * @param dataNasc
     * @param cpf
     * @param rg
     * @param endereco
     * @param naturalidade
     */
    public Usuario(String login, String senha) {
        this.login = login;
        this.senha = senha;
    }

    /**
     * Construtor vazio
     */
    public Usuario() {
    }

    /**
     * Método para retornar login
     *
     * @return
     */
    public String getLogin() {
        return login;
    }

    /**
     * Método para setar login
     *
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Método para retornar senha
     *
     * @return
     */
    public String getSenha() {
        return senha;
    }

    /**
     * Método para setar senha
     *
     * @param senha
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

}
