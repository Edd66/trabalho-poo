/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.ArrayList;

/**
 *
 * @author ideiah
 */
public class Usuarios {

    private ArrayList<Usuario> arrayUsuarios;

    /**
     * Construtor
     * @param arrayUsuarios 
     */
    public Usuarios(ArrayList<Usuario> arrayUsuarios) {
        this.arrayUsuarios = arrayUsuarios;
    }

    /**
     * Construtor que inicia novo array
     */
    public Usuarios() {
        this.arrayUsuarios = new ArrayList<Usuario>();
    }

    public ArrayList<Usuario> getArrayUsuarios() {
        return arrayUsuarios;
    }

    public Usuario getArrayUsuarios(int i) {
        return arrayUsuarios.get(i);
    }

    public void setArrayUsuarios(ArrayList<Usuario> arrayUsuarios) {
        this.arrayUsuarios = arrayUsuarios;
    }

    public void cadastrarUsuarios(Usuario Usuario) {
        this.arrayUsuarios.add(Usuario);
    }

    /**
     * Verifica se ja existe o usuario cadastrado com mesmo nome
     * @param usuario
     * @param usuarios
     * @return 
     */
    public boolean verificarUsuario(String usuario, Usuarios usuarios){
        boolean achou = true;
        for (int i = 0; i < usuarios.getArrayUsuarios().size(); i++) {
            if (usuario.equals(usuarios.getArrayUsuarios().get(i).getLogin())) {
                achou = false;
                break;
            }
        }
        return achou;
    }
    /**
     * Verifica se há campos em branco
     * @param nome
     * @param dataNascimento
     * @param cpf
     * @param rg
     * @param rua
     * @param n
     * @param bairro
     * @param cidade
     * @param senha
     * @param usuario
     * @param estado
     * @return 
     */
    public boolean verificarCampos(String nome, String dataNascimento, String cpf, String rg, String rua, String n, String bairro, String cidade, String senha, String usuario, String estado) {
        boolean bnome = false;
        boolean bdata = false;
        boolean bcpf = false;
        boolean brg = false;
        boolean brua = false;
        boolean bn = false;
        boolean bbairro = false;
        boolean bcidade = false;
        boolean bsenha = false;
        boolean busuario = false;
        boolean cadastrou;

        if (!"".equals(nome)) {
            bnome = true;
        }

        if (!"".equals(dataNascimento)) {
            bdata = true;
        }

        if (!"".equals(cpf)) {
            bcpf = true;
        }

        if (!"".equals(rg)) {
            brg = true;
        }

        if (!"".equals(rua)) {
            brua = true;
        }

        if (!"".equals(n)) {
            bn = true;
        }

        if (!"".equals(bairro)) {
            bbairro = true;
        }

        if (!"".equals(cidade)) {
            bcidade = true;
        }

        if (!"".equals(senha)) {
            bsenha = true;
        }

        if (!"".equals(usuario)) {
            busuario = true;
        }
        
        if (!"".equals(estado)) {
            busuario = true;
        }

        if (bnome == false || bdata == false || bcpf == false || brg == false || brua == false || bn == false || bbairro == false || bcidade == false || bsenha == false || busuario == false) {
            cadastrou = false;
        } else {
            cadastrou = true;
        }
        return cadastrou;
    }

    public void cadastro(){
        arrayUsuarios.add(new Usuario("Eduardo", "131150105"));
        arrayUsuarios.add(new Usuario("admin", "admin"));
    }
}
