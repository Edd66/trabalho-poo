/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author ideiah
 */
public class Endereco {

    private String rua;
    private String num;
    private String bairro;

    /**
     * Consturtor da classe endereço
     *
     * @param rua
     * @param num
     * @param bairro
     */
    public Endereco(String rua, String num, String bairro) {
        this.rua = rua;
        this.num = num;
        this.bairro = bairro;
    }

    /**
     * Construtor vazio
     */
    public Endereco() {
    }

    /**
     * Método para retornar Rua
     *
     * @return rua
     */
    public String getRua() {
        return rua;
    }

    /**
     * método para setar rua
     *
     * @param rua
     */
    public void setRua(String rua) {
        this.rua = rua;
    }

    /**
     * Método para retornar numero
     *
     * @return
     */
    public String getNum() {
        return num;
    }

    /**
     * Método para setar numero
     *
     * @param num
     */
    public void setNum(String num) {
        this.num = num;
    }

    /**
     * Método para retornar bairro
     *
     * @return bairro
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * Método para setar bairro
     *
     * @param bairro
     */
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

}
