/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import javax.swing.JOptionPane;

/**
 *
 * @author ideiah
 */
public class Metodos {

    private Usuarios usuarios;
    private Torcedores torcedores;

    public Metodos(Usuarios usuarios, Torcedores torcedores) {
        this.usuarios = usuarios;
        this.torcedores = torcedores;
    }

    public boolean login(String usuario, String senha) {
        boolean achou = false;
        for (int i = 0; i < usuarios.getArrayUsuarios().size(); i++) {
            if (usuario.equals(usuarios.getArrayUsuarios(i).getLogin())) {
                achou = true;
                break;
            }
        }
        return achou;
    }

    public int pesquisar(String nome) {

        int valor = -1;
        for (int i = 0; i < torcedores.getArrayTorcedores().size(); i++) {
            if (nome.equals(torcedores.getArrayTorcedores().get(i).getNome())) {
                valor = i;
                break;
            }
        }
        return valor;
    }

    public void mostrarTorcedor(Torcedores torcedores, int i) {

        JOptionPane.showMessageDialog(null, "Os dados do torcedor são:\n\n"
                + "Nome: " + torcedores.getArrayTorcedores().get(i).getNome() + "\n"
                + "Data de Nascimento: " + torcedores.getArrayTorcedores().get(i).getDataNasc() + "\n"
                + "CPF: " + torcedores.getArrayTorcedores().get(i).getCpf() + "\n"
                + "RG: " + torcedores.getArrayTorcedores().get(i).getRg() + "\n"
                + "Rua: " + torcedores.getArrayTorcedores().get(i).getEndereco().getRua() + ", nº " + torcedores.getArrayTorcedores().get(i).getEndereco().getNum() + "\n"
                + "Bairro: " + torcedores.getArrayTorcedores().get(i).getEndereco().getBairro() + "\n"
                + "Estado: " + torcedores.getArrayTorcedores().get(i).getNaturalidade().getEstado() + "\n"
                + "Cidade: " + torcedores.getArrayTorcedores().get(i).getNaturalidade().getCidade() + "\n"
                + "Time: " + torcedores.getArrayTorcedores().get(i).getTime() + "\n"
                + "Seleção: " + torcedores.getArrayTorcedores().get(i).getSelecao()+"\n"
                + "Cadastrado por: "+torcedores.getArrayTorcedores().get(i).getUsuarioCadastrou());

    }
    
    public boolean buscarTorcedor(String pesquisar){
        boolean encontrado = false;
        
        for (int i = 0; i < torcedores.getArrayTorcedores().size(); i++) {
            if(pesquisar.equals(torcedores.getArrayTorcedores().get(i).getNome())){
                encontrado = true;
                break;
            }
        }
        return encontrado;
    }
    

}
