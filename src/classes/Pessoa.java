/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 * Classe que cria uma abstração de Pessoa
 *
 * @author ideiah
 */
public class Pessoa {

    private String nome;
    private String dataNasc;
    private String cpf;
    private String rg;
    private Endereco endereco;
    private Naturalidade naturalidade;
    private String usuarioCadastrou;

    /**
     * Construtor da classe pessoa
     *
     * @param nome
     * @param dataNasc
     * @param cpf
     * @param rg
     * @param endereco
     * @param naturalidade
     * @param usuarioCadastrou
     */
    public Pessoa(String nome, String dataNasc, String cpf, String rg, Endereco endereco, Naturalidade naturalidade, String usuarioCadastrou) {
        this.nome = nome;
        this.dataNasc = dataNasc;
        this.cpf = cpf;
        this.rg = rg;
        this.endereco = endereco;
        this.naturalidade = naturalidade;
        this.usuarioCadastrou = usuarioCadastrou;
    }

    /**
     * Construtor da casse pessoa
     *
     * @param nome
     * @param dataNasc
     * @param cpf
     * @param rg
     * @param endereco
     * @param naturalidade
     */
    public Pessoa(String nome, String dataNasc, String cpf, String rg, Endereco endereco, Naturalidade naturalidade) {
        this.nome = nome;
        this.dataNasc = dataNasc;
        this.cpf = cpf;
        this.rg = rg;
        this.endereco = endereco;
        this.naturalidade = naturalidade;
    }

    /**
     * Construtor vazio
     */
    public Pessoa() {
    }

    /**
     * Método para retornar nome
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Método para setar nome
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * método para retornar data de nascimento
     *
     * @return dataNasc
     */
    public String getDataNasc() {
        return dataNasc;
    }

    /**
     * Método para setar data de nascimento
     *
     * @param dataNasc
     */
    public void setDataNasc(String dataNasc) {
        this.dataNasc = dataNasc;
    }

    /**
     * Método para retornar cpf
     *
     * @return cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * Método para setar cpf
     *
     * @param cpf
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * Método para retornar rg
     *
     * @return rg
     */
    public String getRg() {
        return rg;
    }

    /**
     * Método para setar rg
     *
     * @param rg
     */
    public void setRg(String rg) {
        this.rg = rg;
    }

    /**
     * Método para retornar endereço
     *
     * @return endereco
     */
    public Endereco getEndereco() {
        return endereco;
    }

    /**
     * Mpetodo para setar endereço
     *
     * @param endereco
     */
    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    /**
     * Metodo para retornar nacionalidade
     *
     * @return naturalidade
     */
    public Naturalidade getNaturalidade() {
        return naturalidade;
    }

    /**
     * Método para setar naturalidade
     *
     * @param naturalidade
     */
    public void setNaturalidade(Naturalidade naturalidade) {
        this.naturalidade = naturalidade;
    }

    /**
     * Método para retornar usuario
     *
     * @return usuarioCadastrou
     */
    public String getUsuarioCadastrou() {
        return usuarioCadastrou;
    }

    /**
     * Método para setar usuario
     *
     * @param usuarioCadastrou
     */
    public void setUsuarioCadastrou(String usuarioCadastrou) {
        this.usuarioCadastrou = usuarioCadastrou;
    }

}
