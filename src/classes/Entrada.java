package classes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;

/**
 *
 * @author Kai
 */
public class Entrada {
       /**
     * Le um arquivo e retorna as linhas do arquivo dividido em tokens, onde o
     * separador dos tokens as uma virgula.
     *
     * @param filename Nome do arquivo a ser lido.
     * @return Linhas do arquivo dividido em tokens.
     */
    public static ArrayList<ArrayList<String>> getLinhas(String filename) {
        ArrayList<ArrayList<String>> linhas = new ArrayList<>();
        ArrayList<String> linha;
        FileInputStream inFile;
        BufferedReader buff;
        StringTokenizer st;
        String line, token;
        int count;

        try {
            inFile = new FileInputStream(new File(filename));
            buff = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));

            line = buff.readLine();
            while (line != null) {
                st = new StringTokenizer(line, ",");
                count = st.countTokens();
                linha = new ArrayList<String>();
                for (int i = 0; i < count; i++) {
                    token = st.nextToken();
                    linha.add(token.trim());
                }
                linhas.add(linha);
                line = buff.readLine();
            } //end while
            buff.close();
            inFile.close();
        } catch (FileNotFoundException f) {
            System.out.println("Arquivo " + filename + " nÃ£o encontrado.\n" + f.getMessage());
            System.exit(0);
        } catch (Exception e) {
            System.out.println("Error message: " + e.getMessage());
            System.exit(0);
        }
        return linhas;
    }

    public static void escreverArquivo(String nomeDoArquivo, ArrayList<String> linhas) {
        try {
            BufferedWriter escritor = new BufferedWriter(new FileWriter(nomeDoArquivo));
            for (int i = 0; i < linhas.size(); i++) {
                 String linha = linhas.get(i);
                escritor.write(linha + "\n");
            }
            escritor.close();

        } catch (IOException r) {
            JOptionPane.showMessageDialog(null, "Error message: " + r.getMessage());
        }
    }
}
