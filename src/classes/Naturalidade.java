/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author ideiah
 */
public class Naturalidade {

    private String cidade;
    private String estado;

    /**
     * Cosnturtor da classe naturalidade
     *
     * @param cidade
     * @param estado
     */
    public Naturalidade(String cidade, String estado) {
        this.cidade = cidade;
        this.estado = estado;
    }

    /**
     * Construtor vazio
     */
    public Naturalidade() {
    }

    /**
     * Método para retornar cidade
     *
     * @return cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Método para setar cidade
     *
     * @param cidade
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * Método para retornar estado
     *
     * @return estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Método para setar estado
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

}
