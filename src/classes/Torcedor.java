/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author ideiah
 */
public class Torcedor extends Pessoa {

    private String time;
    private String selecao;

    /**
     * Construtor da classe torcedor
     *
     * @param time
     * @param selecao
     * @param nome
     * @param dataNasc
     * @param cpf
     * @param rg
     * @param endereco
     * @param naturalidade
     * @param usuarioCadastrou
     */
    public Torcedor(String time, String selecao, String nome, String dataNasc, String cpf, String rg, Endereco endereco, Naturalidade naturalidade, String usuarioCadastrou) {
        super(nome, dataNasc, cpf, rg, endereco, naturalidade, usuarioCadastrou);
        this.time = time;
        this.selecao = selecao;
    }

    /**
     * Construtor vazio
     */
    public Torcedor() {
    }

    /**
     * Método para retornar time
     *
     * @return time
     */
    public String getTime() {
        return time;
    }

    /**
     * Método para setar time
     *
     * @param time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * Método para retornar selecao
     *
     * @return selecao
     */
    public String getSelecao() {
        return selecao;
    }

    /**
     * Metodo para setar selecao
     *
     * @param selecao
     */
    public void setSelecao(String selecao) {
        this.selecao = selecao;
    }

}
