package interfaces;

import java.awt.Container;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import classes.Metodos;
import classes.Torcedores;
import classes.Usuarios;

public class MenuFrame extends JFrame implements ActionListener {

    // Variáveis da classe
    private String pesquisar;
    private Usuarios usuarios;
    private String nome;
    private Torcedores torcedores;
    private Metodos m;

    // Variáveis Interface
    private JButton bCadastrar, bSearch, jButton1, jButton2, jButton3;
    private JTextField cPesquisar;
    private JLabel jLabel1, jLabel2, jLabel3, jLabel4, jLabel5, jLabel6;

    /**
     * Cria um novo Frame
     */
    public MenuFrame(Usuarios usuarios, String nome, Torcedores torcedores,
            Metodos m) {
        super("Menu Principal");
        componentesIniciais();
        this.usuarios = usuarios;
        this.nome = nome;
        this.torcedores = torcedores;
        this.m = m;
        jLabel1.setText("Olá " + nome + ", escolha a opção desejada abaixo: ");

    }

    /**
     * Método que contem a interface, campos de texto, botões
     */
    public void componentesIniciais() {
        Container content = getContentPane();
        content.setLayout(null);

        // Apresentação
        jLabel1 = new JLabel();
        jLabel1.setText("Bem-vindo");
        jLabel1.setBounds(new Rectangle(10, 10, 300, 15));
        content.add(jLabel1);

        //Label cadastro
        jLabel2 = new JLabel();
        jLabel2.setText("Clique aqui para cadastro de torcedores:");
        jLabel2.setBounds(new Rectangle(10, 50, 300, 15));
        content.add(jLabel2);

        //Botão Cadastrar
        bCadastrar = new JButton();
        bCadastrar.setText("Cadastrar");
        bCadastrar.setActionCommand("Cadastrar");
        bCadastrar.addActionListener(this);
        bCadastrar.setBounds(new Rectangle(115, 70, 120, 28));
        content.add(bCadastrar);

        //Label Exibir
        jLabel3 = new JLabel();
        jLabel3.setText("Clique aqui para exibir os dados:");
        jLabel3.setBounds(new Rectangle(10, 110, 300, 15));
        content.add(jLabel3);

        //Botão exibir
        jButton2 = new JButton();
        jButton2.setText("Exibir");
        jButton2.setActionCommand("Exibir");
        jButton2.addActionListener(this);
        jButton2.setBounds(new Rectangle(115, 130, 120, 28));
        content.add(jButton2);

        //Label Editar
        jLabel4 = new JLabel();
        jLabel4.setText("Clique aqui para editar:");
        jLabel4.setBounds(new Rectangle(10, 170, 300, 15));
        content.add(jLabel4);

        //Botão Editar
        jButton3 = new JButton();
        jButton3.setText("Editar:");
        jButton3.setActionCommand("Editar");
        jButton3.addActionListener(this);
        jButton3.setBounds(new Rectangle(115, 190, 120, 28));
        content.add(jButton3);

        //Label Pesquisar
        jLabel5 = new JLabel();
        jLabel5.setText("Ou pesquise por um nome aqui:");
        jLabel5.setBounds(new Rectangle(10, 230, 300, 15));
        content.add(jLabel5);

        //Label Pesquisar2
        jLabel6 = new JLabel();
        jLabel6.setText("Pesquisar: ");
        jLabel6.setBounds(new Rectangle(10, 260, 300, 15));
        content.add(jLabel6);

        //Campo Pesquisar
        cPesquisar = new JTextField();
        cPesquisar.setBounds(new Rectangle(90, 255, 150, 28));
        content.add(cPesquisar);

        //Botão Pesquisar
        bSearch = new JButton();
        bSearch.setText("Search");
        bSearch.setActionCommand("Search");
        bSearch.addActionListener(this);
        bSearch.setBounds(new Rectangle(250, 255, 100, 28));
        content.add(bSearch);

        //Botão Logout
        jButton1 = new JButton();
        jButton1.setText("Logout");
        jButton1.setActionCommand("Logout");
        jButton1.addActionListener(this);
        jButton1.setBounds(new Rectangle(300, 300, 100, 28));
        content.add(jButton1);

        // Comando que torna o Frame visível
        setVisible(true);
        // Define o tamanho do Frame
        setSize(420, 380);
        // Inicializa o Frame no centro da tela
        setLocationRelativeTo(null);
    }

    /**
     * Método que contem as ações de execução de seus respectivos botões
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = (String) e.getActionCommand();

        if ("Cadastrar".equals(comando)) {
            this.dispose();
            new CadastroTorcedor(torcedores, usuarios, nome, m).setVisible(true);
        }
        if ("Exibir".equals(comando)) {
            if (torcedores.getArrayTorcedores().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Nenhum torcedor foi cadastrado!");
            } else {
                for (int i = 0; i < torcedores.getArrayTorcedores().size(); i++) {
                    m.mostrarTorcedor(torcedores, i);
                }
            }
        }
        if ("Editar".equals(comando)) {
            if (torcedores.getArrayTorcedores().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Nenhum torcedor foi cadastrado!");
            } else {
                pesquisar = JOptionPane.showInputDialog("Digite o nome do torcedor que deseja alterar:");
                try {
                    pesquisar.toString();
                } catch (NullPointerException ex) {
                    return;
                }
                if (m.buscarTorcedor(pesquisar) == true) {
                    this.dispose();
                    new EditarTorcedor(torcedores, usuarios, nome, m, m.pesquisar(pesquisar)).setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null, "Torcedor não encontrado!");
                }
            }
        }
        if ("Search".equals(comando)) {
            int i;
            pesquisar = cPesquisar.getText();
            cPesquisar.setText("");
            if (m.pesquisar(pesquisar) == -1) {
                JOptionPane.showMessageDialog(null, "O nome não foi encontrado!");
            } else {
                i = m.pesquisar(pesquisar);
                m.mostrarTorcedor(torcedores, i);
            }
        }
        if ("Logout".equals(comando)) {
            this.dispose();
            new LoginFrame(usuarios, torcedores).setVisible(true);
        }

    }

}
