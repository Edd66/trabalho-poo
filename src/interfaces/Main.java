
package interfaces;

import classes.Torcedores;
import classes.Usuarios;

/**
 *
 * @author ideiah
 */
public class Main {
    
    public static void main(String[] args) {
        Usuarios usuarios = new Usuarios();
        Torcedores torcedores = new Torcedores();
        usuarios.cadastro();
        new LoginFrame(usuarios, torcedores);
    }
}
