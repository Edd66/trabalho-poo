package interfaces;

import java.awt.Container;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;

import classes.Metodos;
import classes.Torcedores;
import classes.Usuarios;

public class LoginFrame extends JFrame implements ActionListener {
	// Variáveis da classe
	private Metodos m;
	private Usuarios usuarios;
	private Torcedores torcedores;

	// Interface
	private JLabel jLabel1, jLabel2, jLabel3, jLabel4;
	private JTextField cUsuario;
	private JButton bEntrar, bCadastrar;
	private JPasswordField cSenha;

	/**
	 * Construtor que cria um novo LoginFrame
	 */
	public LoginFrame(Usuarios usuarios, Torcedores torcedores) {
		super("Login");
		componentesIniciais();
		this.usuarios = usuarios;
		this.m = new Metodos(usuarios, torcedores);
		this.torcedores = torcedores;
                getRootPane().setDefaultButton(bEntrar); 
	}

	/**
	 * Método onde são iniciados os componentes no Frame
	 */
	public void componentesIniciais() {
		Container content = getContentPane();
		content.setLayout(null);

		// Label Apresentação
		jLabel1 = new JLabel();
		jLabel1.setText("Faça login abaixo para utilizar o programa:");
		jLabel1.setBounds(new Rectangle(10/* largura */, 10/* altura */,
				400/* largura */, 15/* altura */));
		content.add(jLabel1);

		// Label Usuário
		jLabel2 = new JLabel();
		jLabel2.setText("Usuário:");
		jLabel2.setBounds(new Rectangle(10, 50, 80, 15));
		content.add(jLabel2);

		// Campo Usuário
		cUsuario = new JTextField();
		cUsuario.setBounds(new Rectangle(75, 45, 250, 28));
		content.add(cUsuario);

		// Label Senha
		jLabel3 = new JLabel();
		jLabel3.setText("Senha:");
		jLabel3.setBounds(new Rectangle(10, 90, 80, 15));
		content.add(jLabel3);

		// Campo Senha
		cSenha = new JPasswordField();
		cSenha.setBounds(new Rectangle(75, 85, 250, 28));
		content.add(cSenha);

		// Botão Entrar
		bEntrar = new JButton();
		bEntrar.setText("Entrar");
		bEntrar.setBounds(new Rectangle(150, 120, 80, 28));
		bEntrar.setActionCommand("Entrar");
		bEntrar.addActionListener(this);
		content.add(bEntrar);


		// Comando que torna o Frame visível
		setVisible(true);
		// Define o tamanho do Frame
		setSize(380, 210);
		// Inicializa o Frame no centro da tela
		setLocationRelativeTo(null);
	}

	public static void main(Usuarios usuarios, Torcedores torcedores) {
		new LoginFrame(usuarios, torcedores);
	}

        /**
         * Método que contem as ações de execução de seus respectivos botões
         * @param e 
         */
        @Override
	public void actionPerformed(ActionEvent e) {
		String comando = (String) e.getActionCommand();
		if ("Entrar".equals(comando)) {
			String usuario = cUsuario.getText();
			String senha = new String(cSenha.getPassword());

			if (m.login(usuario, senha) == true) {
				this.dispose();
				new MenuFrame(usuarios, cUsuario.getText(), torcedores, m).setVisible(true);
			} else {
				JOptionPane.showMessageDialog(rootPane,"Login ou senha foram escritos incorretamente!");
				cUsuario.setText("");
				cSenha.setText("");
			}
		}
	}
}
