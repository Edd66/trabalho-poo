package interfaces;

import java.awt.Container;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import classes.Endereco;
import classes.Entrada;
import classes.Metodos;
import classes.Naturalidade;
import classes.Torcedor;
import classes.Torcedores;
import classes.Usuarios;

public class CadastroTorcedor extends JFrame implements ActionListener {

    private Metodos m;
    private Usuarios usuarios;
    private Torcedores torcedores;
    private Torcedor objTorcedor;
    private String nomeUsuario;
    private boolean exception;

    private JButton bCadastrar;
    private JTextField cBairro;
    private JTextField cCidade;
    private JTextField cCpf;
    private JTextField cData;
    private JTextField cEstado;
    private JTextField cN;
    private JTextField cNome;
    private JTextField cRg;
    private JTextField cRua;
    private JTextField cSelecao;
    private JTextField cTime;
    private JButton jButton1;
    private JLabel jLabel1;
    private JLabel jLabel10;
    private JLabel jLabel11;
    private JLabel jLabel12;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;

    /**
     * Cria um novo Frame
     */
    public CadastroTorcedor(Torcedores torcedores, Usuarios usuarios,
            String nomeUsuario, Metodos m) {
        super("Cadastro de Torcedores");
        componentesIniciais();
        this.torcedores = torcedores;
        this.usuarios = usuarios;
        this.nomeUsuario = nomeUsuario;
        this.m = m;
        exception = false;
    }

    /**
     * Método que contem a interface, campos de texto, botões
     */
    public void componentesIniciais() {
        Container content = getContentPane();
        content.setLayout(null);

        // Label Apresentação
        jLabel1 = new JLabel();
        jLabel1.setText("Preencha os dados abaixo para cadastro no sistema:");
        jLabel1.setBounds(new Rectangle(10/* largura */, 10/* altura */,
                400/* largura */, 15/* altura */));
        content.add(jLabel1);

        // Label Nome
        jLabel2 = new JLabel();
        jLabel2.setText("Nome completo:");
        jLabel2.setBounds(new Rectangle(10, 50, 120, 15));
        content.add(jLabel2);

        // Campo Nome
        cNome = new JTextField();
        cNome.setBounds(new Rectangle(130, 45, 250, 28));
        content.add(cNome);

        // Label Data de Nascimento
        jLabel3 = new JLabel();
        jLabel3.setText("Data de Nascimento:");
        jLabel3.setBounds(new Rectangle(10, 90, 170, 15));
        content.add(jLabel3);

        // Campo Data de Nascimento
        cData = new JTextField();
        cData.setBounds(new Rectangle(160, 85, 250, 28));
        content.add(cData);

        // Label CPF
        jLabel4 = new JLabel();
        jLabel4.setText("CPF:");
        jLabel4.setBounds(new Rectangle(10, 130, 170, 15));
        content.add(jLabel4);

        // Campo CPF
        cCpf = new JTextField();
        cCpf.setBounds(new Rectangle(60, 125, 250, 28));
        content.add(cCpf);

        // Label RG
        jLabel5 = new JLabel();
        jLabel5.setText("RG:");
        jLabel5.setBounds(new Rectangle(10, 170, 170, 15));
        content.add(jLabel5);

        // Campo RG
        cRg = new JTextField();
        cRg.setBounds(new Rectangle(60, 165, 250, 28));
        content.add(cRg);

        // Label Bairro
        jLabel6 = new JLabel();
        jLabel6.setText("Bairro:");
        jLabel6.setBounds(new Rectangle(10, 210, 170, 15));
        content.add(jLabel6);

        // Campo Bairro
        cBairro = new JTextField();
        cBairro.setBounds(new Rectangle(60, 205, 125, 28));
        content.add(cBairro);

        // Label Rua
        jLabel7 = new JLabel();
        jLabel7.setText("Rua:");
        jLabel7.setBounds(new Rectangle(190, 210, 170, 15));
        content.add(jLabel7);

        // Campo Rua
        cRua = new JTextField();
        cRua.setBounds(new Rectangle(225, 205, 125, 28));
        content.add(cRua);

        // Label Numero
        jLabel8 = new JLabel();
        jLabel8.setText("Nº:");
        jLabel8.setBounds(new Rectangle(350, 210, 170, 15));
        content.add(jLabel8);

        // Campo Numero
        cN = new JTextField();
        cN.setBounds(new Rectangle(380, 205, 70, 28));
        content.add(cN);

        // Label Estado
        jLabel9 = new JLabel();
        jLabel9.setText("Estado:");
        jLabel9.setBounds(new Rectangle(10, 250, 170, 15));
        content.add(jLabel9);

        // Campo Estado
        cEstado = new JTextField();
        cEstado.setBounds(new Rectangle(70, 245, 150, 28));
        content.add(cEstado);

        // Label Cidade
        jLabel10 = new JLabel();
        jLabel10.setText("Cidade:");
        jLabel10.setBounds(new Rectangle(10, 290, 170, 15));
        content.add(jLabel10);

        // Campo Cidade
        cCidade = new JTextField();
        cCidade.setBounds(new Rectangle(70, 285, 200, 28));
        content.add(cCidade);

        // Label Seleção
        jLabel11 = new JLabel();
        jLabel11.setText("Seleção:");
        jLabel11.setBounds(new Rectangle(10, 330, 170, 15));
        content.add(jLabel11);

        // Campo Seleção
        cSelecao = new JTextField();
        cSelecao.setBounds(new Rectangle(80, 325, 200, 28));
        content.add(cSelecao);

        // Label Time:
        jLabel11 = new JLabel();
        jLabel11.setText("Time:");
        jLabel11.setBounds(new Rectangle(10, 370, 170, 15));
        content.add(jLabel11);

        // Campo Time
        cTime = new JTextField();
        cTime.setBounds(new Rectangle(80, 365, 200, 28));
        content.add(cTime);

        // Botão Cadastrar
        bCadastrar = new JButton();
        bCadastrar.setText("Cadastrar");
        bCadastrar.setActionCommand("Cadastrar");
        bCadastrar.addActionListener(this);
        bCadastrar.setBounds(new Rectangle(120, 415, 120, 28));
        content.add(bCadastrar);

        // Botão Voltar
        jButton1 = new JButton();
        jButton1.setText("Voltar");
        jButton1.setActionCommand("Voltar");
        jButton1.addActionListener(this);
        jButton1.setBounds(new Rectangle(260, 415, 120, 28));
        content.add(jButton1);

        // Comando que torna o Frame visível
        setVisible(true);
        // Define o tamanho do Frame
        setSize(500, 500);
        // Inicializa o Frame no centro da tela
        setLocationRelativeTo(null);

    }

    /**
     * Método que contem as ações de execução de seus respectivos botões
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = (String) e.getActionCommand();
        String mensagem = "";

        if ("Cadastrar".equals(comando)) {

            if (usuarios.verificarCampos(cNome.getText(), cData.getText(), cRg.getText(), cRg.getText(), cRua.getText(), cN.getText(), cBairro.getText(),
                    cCidade.getText(), cTime.getText(), cSelecao.getText(), cEstado.getText()) == false) {
                JOptionPane.showMessageDialog(rootPane,
                        "Por favor, preencha todos os campos!");

            } else {

                try {
                    Integer.parseInt(cN.getText());
                } catch (NumberFormatException ex) {
                    mensagem += "Campo número inválido!\n";
                }
                try {
                    Integer.parseInt(cRg.getText());
                } catch (NumberFormatException ex) {
                    mensagem += "Campo Rg inválido!\n";
                }
                try {
                    Integer.parseInt(cCpf.getText());
                } catch (NumberFormatException ex) {
                    mensagem += "Campo CPF inválido!\n";
                }
                if ("".equals(mensagem)) {
                    Naturalidade naturalidade = new Naturalidade(cCidade.getText(), cEstado.getText());
                    Endereco endereco = new Endereco(cRua.getText(), cN.getText(), cBairro.getText());

                    objTorcedor = new Torcedor(cTime.getText(), cSelecao.getText(), cNome.getText(), cData.getText(), cCpf.getText(), cRg.getText(),
                            endereco, naturalidade, nomeUsuario);
                    torcedores.cadastrarTorcedores(objTorcedor);
                    this.dispose();
                    JOptionPane.showMessageDialog(rootPane,
                            "Torcedor cadastrado com sucesso!");
                    new MenuFrame(usuarios, nomeUsuario, torcedores, m)
                            .setVisible(true);
                    Entrada.escreverArquivo("src/classes/Torcedores.txt", torcedores.escrever());
                } else {
                    JOptionPane.showMessageDialog(null, mensagem);
                }
            }
        }
        if ("Voltar".equals(comando)) {
            this.dispose();
            new MenuFrame(usuarios, nomeUsuario, torcedores, m).setVisible(true);
        }

    }

}
