package interfaces;

import java.awt.Button;
import java.awt.Container;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import classes.Endereco;
import classes.Metodos;
import classes.Naturalidade;
import classes.Torcedores;
import classes.Usuario;
import classes.Usuarios;

public class CadastroUsuario extends JFrame implements ActionListener {

    // Variáveis da classe
    private Metodos m;
    private Torcedores torcedores;
    private Usuarios usuarios;
    private Usuario objUsuario;

    // Interface
    private JButton bCadastrar, jButton1;
    private JTextField cBairro, cCidade, cCpf, cData, cN, cNome, cRg, cRua,
            cSenha, cUsuario, cEstado;
    private JLabel jLabel1, jLabel2, jLabel3, jLabel4, jLabel5, jLabel6,
            jLabel7, jLabel8, jLabel9, jLabel10, jLabel11, jLabel12;

    /**
     * Construtor que cria um novo Frame
     */
    public CadastroUsuario(Usuarios usuarios, Torcedores torcedores) {
        super("Cadastro de Usuários");
        componentesIniciais();
        this.usuarios = usuarios;
        this.torcedores = torcedores;
        this.m = new Metodos(usuarios, torcedores);
    }

    /**
     * Método onde são iniciados os componentes no Frame
     */
    public void componentesIniciais() {
        Container content = getContentPane();
        content.setLayout(null);

        // Label Apresentação
        jLabel1 = new JLabel();
        jLabel1.setText("Preencha os dados abaixo para cadastro no sistema:");
        jLabel1.setBounds(new Rectangle(10/* largura */, 10/* altura */,
                400/* largura */, 15/* altura */));
        content.add(jLabel1);

        // Label Nome
        jLabel2 = new JLabel();
        jLabel2.setText("Nome completo:");
        jLabel2.setBounds(new Rectangle(10, 50, 120, 15));
        content.add(jLabel2);

        // Campo Nome
        cNome = new JTextField();
        cNome.setBounds(new Rectangle(130, 45, 250, 28));
        content.add(cNome);

        // Label Data de Nascimento
        jLabel3 = new JLabel();
        jLabel3.setText("Data de Nascimento:");
        jLabel3.setBounds(new Rectangle(10, 90, 170, 15));
        content.add(jLabel3);

        // Campo Data de Nascimento
        cData = new JTextField();
        cData.setBounds(new Rectangle(160, 85, 250, 28));
        content.add(cData);

        // Label CPF
        jLabel4 = new JLabel();
        jLabel4.setText("CPF:");
        jLabel4.setBounds(new Rectangle(10, 130, 170, 15));
        content.add(jLabel4);

        // Campo CPF
        cCpf = new JTextField();
        cCpf.setBounds(new Rectangle(60, 125, 250, 28));
        content.add(cCpf);

        // Label RG
        jLabel5 = new JLabel();
        jLabel5.setText("RG:");
        jLabel5.setBounds(new Rectangle(10, 170, 170, 15));
        content.add(jLabel5);

        // Campo RG
        cRg = new JTextField();
        cRg.setBounds(new Rectangle(60, 165, 250, 28));
        content.add(cRg);

        // Label Bairro
        jLabel6 = new JLabel();
        jLabel6.setText("Bairro:");
        jLabel6.setBounds(new Rectangle(10, 210, 170, 15));
        content.add(jLabel6);

        // Campo Bairro
        cBairro = new JTextField();
        cBairro.setBounds(new Rectangle(60, 205, 125, 28));
        content.add(cBairro);

        // Label Rua
        jLabel7 = new JLabel();
        jLabel7.setText("Rua:");
        jLabel7.setBounds(new Rectangle(190, 210, 170, 15));
        content.add(jLabel7);

        // Campo Rua
        cRua = new JTextField();
        cRua.setBounds(new Rectangle(225, 205, 125, 28));
        content.add(cRua);

        // Label Numero
        jLabel8 = new JLabel();
        jLabel8.setText("Nº:");
        jLabel8.setBounds(new Rectangle(350, 210, 170, 15));
        content.add(jLabel8);

        // Campo Numero
        cN = new JTextField();
        cN.setBounds(new Rectangle(380, 205, 70, 28));
        content.add(cN);

        // Label Estado
        jLabel9 = new JLabel();
        jLabel9.setText("Estado:");
        jLabel9.setBounds(new Rectangle(10, 250, 170, 15));
        content.add(jLabel9);

        // Campo Estado
        cEstado = new JTextField();
        cEstado.setBounds(new Rectangle(70, 245, 150, 28));
        content.add(cEstado);

        // Label Cidade
        jLabel10 = new JLabel();
        jLabel10.setText("Cidade:");
        jLabel10.setBounds(new Rectangle(10, 290, 170, 15));
        content.add(jLabel10);

        // Campo Cidade
        cCidade = new JTextField();
        cCidade.setBounds(new Rectangle(70, 285, 200, 28));
        content.add(cCidade);

        // Label Usuário
        jLabel11 = new JLabel();
        jLabel11.setText("Usuário:");
        jLabel11.setBounds(new Rectangle(10, 330, 170, 15));
        content.add(jLabel11);

        // Campo Usuário
        cUsuario = new JTextField();
        cUsuario.setBounds(new Rectangle(80, 325, 200, 28));
        content.add(cUsuario);

        // Label Senha
        jLabel11 = new JLabel();
        jLabel11.setText("Senha:");
        jLabel11.setBounds(new Rectangle(10, 370, 170, 15));
        content.add(jLabel11);

        // Campo Senha
        cSenha = new JPasswordField();
        cSenha.setBounds(new Rectangle(80, 365, 200, 28));
        content.add(cSenha);

        // Botão Cadastrar
        bCadastrar = new JButton();
        bCadastrar.setText("Cadastrar");
        bCadastrar.setActionCommand("Cadastrar");
        bCadastrar.addActionListener(this);
        bCadastrar.setBounds(new Rectangle(120, 415, 120, 28));
        content.add(bCadastrar);

        // Botão Voltar
        jButton1 = new JButton();
        jButton1.setText("Voltar");
        jButton1.setActionCommand("Voltar");
        jButton1.addActionListener(this);
        jButton1.setBounds(new Rectangle(260, 415, 120, 28));
        content.add(jButton1);

        // Comando que torna o Frame visível
        setVisible(true);
        // Define o tamanho do Frame
        setSize(500, 500);
        // Inicializa o Frame no centro da tela
        setLocationRelativeTo(null);
    }

    /**
     * Método que contem as ações de execução de seus respectivos botões
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = (String) e.getActionCommand();
        if ("Cadastrar".equals(comando)) {

            if (usuarios.verificarUsuario(cUsuario.getText(), usuarios) == false) {
                JOptionPane.showMessageDialog(null,
                        "Este nome de usuário não está disponível");
            } else {

                if (usuarios.verificarCampos(cNome.getText(), cData.getText(), cCpf.getText(), cRg.getText(), cRua.getText(), cN.getText(),
                        cBairro.getText(), cCidade.getText(), cSenha.getText(), cUsuario.getText(), cEstado.getText()) == false) {
                    JOptionPane.showMessageDialog(rootPane,
                            "Por favor, preencha todos os campos!");

                } else {
                    Naturalidade naturalidade = new Naturalidade(cCidade.getText(), cEstado.getText());
                    Endereco endereco = new Endereco(cRua.getText(), cN.getText(), cBairro.getText());
                    objUsuario = new Usuario(cUsuario.getText(), cSenha.getText());
                    usuarios.cadastrarUsuarios(objUsuario);
                    this.dispose();
                    LoginFrame.main(usuarios, torcedores);
                    JOptionPane
                            .showMessageDialog(rootPane,
                                    "Cadastro concluído com êxito! \nEntre com seu usuário e senha:");
                }

            }

        }
        if ("Voltar".equals(comando)) {
            this.dispose();
            new LoginFrame(usuarios, torcedores);
        }

    }

}
